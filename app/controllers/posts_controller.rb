class PostsController < ApplicationController
  def index
    @posts = Post.all
    render :json => @posts
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      render :json => @post
    else
      render :json => @post.errors.full_messages, :status => 422
    end
  end

  def update
    @post = Post.find(params[:id])
    if @post
      if @post.update_attributes(post_params)
        render :json => @post
      else
        render :json => @post.errors.full_messages, :status => 422
      end
    else
      head 404
    end
  end

  def destroy
    @post = Post.find(params[:id])
    if @post
      if @post.destroy
        render :json => @post
      else
        render :json => @post, :status => 422
      end
    else
      head 404
    end
  end

  def show
    @post = Post.find(params[:id])
    if @post
      render :json => @post
    else
      head 404
    end
  end

  def start_app
    render :index
  end

  private
  def post_params
    params.require(:post).permit(:title, :body)
  end
end
