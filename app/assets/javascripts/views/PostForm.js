JournalApp.Views.PostForm = Backbone.View.extend({

  initialize: function(options) {
    this.listenTo(this.model, "change update", this.render);
  },

  template: JST["post_form"],

  events: {
    'click #submit-new-post' : 'submitPost'
  },

  submitPost: function(event) {

    event.preventDefault();
    var data = $('#new-post-form').serializeJSON()["post"]
    var formView = this;

    this.model.save(data, {
      success: function(model) {
        if (JournalApp.Collections.posts.get(model.id)) {
          Backbone.history.navigate("#/posts/" + model.id);
        } else {
          JournalApp.Collections.posts.add(model);
          Backbone.history.navigate("#/");
        }
      },
      error: function(model, errors) {
        formView.render();
        formView.displayErrors(JSON.parse(errors.responseText));
      }
    });
  },

  render: function(){
    this.$el.html( this.template({ attrs: this.model.attributes }) );
    return this;
  },

  displayErrors: function(errors){
    (this.$el).prepend(JST["errors"]({errors: errors}))
  }
});