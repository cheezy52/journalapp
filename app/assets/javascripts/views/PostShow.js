JournalApp.Views.PostShow = Backbone.View.extend({
  template: JST["post_show"],

  initialize: function(options) {
    this.listenTo(this.model, "change update", this.render)
  },

  events: {
    "dblclick h2" : "showTitleEditBox",
    "dblclick .postBody" : "showBodyEditBox",
    "blur #title" : "submitNewTitle",
    "blur #body" : "submitNewBody",
    "keypress #title" : "keypressSubmitNewTitle",
    "keypress #body" : "keypressSubmitNewBody"
  },

  render: function(){
    this.$el.html( this.template({ post: this.model }) );
    return this;
  },

  showTitleEditBox: function(event){
    var text = $(event.target).text();
    $(event.target).html($('<input type="text" id="title" value="' + text +'"></input>'));
    $('#title').focus();
  },

  submitNewTitle: function(event){
    this.model.save( {title: $(event.target).val()}, {
      success: function(model){
        $(event.target).parent().html(model.escape('title'));
      }
    })
  },

  showBodyEditBox: function(){
    var text = $(event.target).text();
    $(event.target).html($('<input type="text" id="body" value="' + text +'"></input>'));
    $('#body').focus();
  },

  submitNewBody: function(event){
    this.model.save( {body: $(event.target).val()}, {
      success: function(model){
        $(event.target).parent().html(model.escape('body'));
      }
    })
  },

  keypressSubmitNewTitle: function(event) {
    if (event.which === 13) {
      this.model.save( {title: $(event.target).val()}, {
        success: function(model){
          $(event.target).parent().html(model.escape('title'));
        }
      })
    }
  },

  keypressSubmitNewBody: function(event) {
    if (event.which === 13) {
      this.model.save( {body: $(event.target).val()}, {
        success: function(model){
          $(event.target).parent().html(model.escape('body'));
        }
      })
    }
  }

});