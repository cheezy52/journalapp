JournalApp.Views.PostsIndex = Backbone.View.extend({
  template: JST["posts_index"],

  initialize: function(options) {
    this.listenTo(this.collection, "add sync remove reset change:title", this.render)
  },

  events: {
    'click button' : 'deletePost'
  },

  render: function(){
    this.$el.html( this.template({ posts: this.collection }) );
    return this;
  },

  deletePost: function(event){
    var post = this.collection.get($(event.target).data("id"));
    post.destroy();
  }

});