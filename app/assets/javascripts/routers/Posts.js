JournalApp.Routers.Posts = Backbone.Router.extend({
  //Remember to pass in something named "$el" on construction!

  routes: {
    '': 'index',
    'posts': 'index',
    'posts/new': 'new',
    'posts/:id': 'show',
    'posts/:id/edit' : 'edit'
  },

  index: function(){
    var view = new JournalApp.Views.PostsIndex({
      collection: JournalApp.Collections.posts
    });
    this._swapView(view);
  },

  show: function(id){
    var view = new JournalApp.Views.PostShow({
      collection: JournalApp.Collections.posts,
      model: JournalApp.Collections.posts.getOrFetch(id)
    });
    this._swapView(view);
  },

  new: function(){
    var view = new JournalApp.Views.PostForm({
      collection: JournalApp.Collections.posts,
      model: new JournalApp.Models.Post()
    });
    this._swapView(view);
  },

  edit: function(id){
    var view = new JournalApp.Views.PostForm({
      collection: JournalApp.Collections.posts,
      model: JournalApp.Collections.posts.getOrFetch(id)
    });
    this._swapView(view);
  },

  _swapView: function(view) {
    $(this.$el).empty();
    $(this.$el).html(view.render().$el);
  }

});