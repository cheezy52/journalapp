window.JournalApp = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function() {

    var router = new JournalApp.Routers.Posts();
    router.$el = $('div#content');
    Backbone.history.start();

    JournalApp.Collections.posts.fetch();
    var sidebarView = new JournalApp.Views.PostsIndex({
      collection: JournalApp.Collections.posts
    });
    $('div#sidebar').html(sidebarView.render().$el);

  }
};

$(document).ready(function(){
  JournalApp.initialize();
});
