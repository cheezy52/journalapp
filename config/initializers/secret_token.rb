# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
JournalApp::Application.config.secret_key_base = '89af14fb50e9186a1543bd3428fc06e41b06bb8e74dddad2adc0d113a734481359d5b869d04b68a5c19b6300d2112ad455f0b608c1b014cbc4dbd31ccf1ad1ef'
