JournalApp::Application.routes.draw do
  root to: "posts#start_app"
  resources :posts, only: [:index, :create, :destroy, :show, :update]
end
